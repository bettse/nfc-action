const {
  NFC,
  CONNECT_MODE_CARD,
  SCARD_PROTOCOL_T0,
  SCARD_PROTOCOL_T1,
} = require("nfc-pcsc");

const fs = require("fs");
const { spawn } = require("child_process");
const shell = require("default-shell");

const { DEBUG } = process.env;

const reader_name = "ACS ACR1252 1S CL Reader(1)";

const nfc = new NFC(); // optionally you can pass logger

function tag_event(tag, direction) {
  const stdio = DEBUG ? "inherit" : "ignore";
  const options = {
    // cwd: User's home
    shell,
    detached: true,
    stdio,
  };

  try {
    // Reload config file each time incase it changed
    const configFile = fs.readFileSync("./config.json");
    const config = JSON.parse(configFile);

    const tagConfig = config[tag] || config[tag.toUpperCase()];

    if (!tagConfig) {
      console.log(`> no env or config command for ${tag}`);
      const process = spawn(`say unknown tag ${direction}`, [], options);
      return;
    }
    const directionCommand = tagConfig[direction];
    const command =
      typeof tagConfig === "string" ? tagConfig : directionCommand;
    // TODO: Support object commands which specify working directory, shell, etc
    // TODO: Support base64 encoded object commands for adding to process.env
    if (command) {
      console.log(`> ${tag} => ${command}`);
      const process = spawn(command, [], options);
      process.on("error", console.error);
    } else {
      console.log(`> no ${direction} command for ${tag}`);
    }
  } catch (e) {
    console.log(e);
    return;
  }
}

nfc.on("reader", (reader) => {
  reader.autoProcessing = reader.reader.name.startsWith(reader_name);
  if (reader.reader.name.startsWith(reader_name)) {
    console.log(`${reader.reader.name} device attached`);
  } else {
    console.log(`${reader.reader.name} device attached: ignoring`);
    return;
  }

  reader.on("card", (card) => {
    //console.log(`${reader.reader.name} card detected`, card);
    tag_event(card.uid, "enter");
  });

  reader.on("card.off", (card) => {
    //console.log(`${reader.reader.name} card removed`, card);
    tag_event(card.uid, "exit");
  });

  reader.on("error", (err) => {
    console.log(`${reader.reader.name} an error occurred`, err);
  });

  reader.on("end", () => {
    console.log(`${reader.reader.name} device removed`);
  });
});

nfc.on("error", (err) => {
  console.log("an error occurred", err);
});
