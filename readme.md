# nfc-action

Trigger a command vis a UID detected by an NFC reader.

Based on https://gitlab.com/bettse/rfid-action but using a `nfc-pcsc` compatible reader.

Rename `config-example.json` to `config.json` and setup any UIDs you'd like to trigger based on.


## TODO:

 * Allow pcsc device name to be specified in config or ENV
